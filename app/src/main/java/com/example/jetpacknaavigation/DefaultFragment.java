package com.example.jetpacknaavigation;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DefaultFragment extends Fragment {

    NavController navController;
    private RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    ArrayList<Pokemon_> parrayList;

    public DefaultFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("Default Fragment :","OnCreate Called!");

        navController = Navigation.findNavController(getActivity(),R.id.nav_host_fragment);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("Default Fragment :","OnCreateView Called!");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_default, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("Default Fragment :","OnViewCreated Called!");


        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        Call<Pokemon> call = service.getAllPokemons();

        call.enqueue(new Callback<Pokemon>() {
            @Override
            public void onResponse(Call<Pokemon> call, Response<Pokemon> response) {

                System.out.println("Response From URL :" + response.body());

                try {
                    Pokemon pokemon = response.body();

                    parrayList = new ArrayList<>(pokemon.getPokemon());

                    Log.d("Arraylist size :","Size :"+parrayList.size());

                    generateView(parrayList,view);

                }catch (NullPointerException e)
                {
                    System.out.println("Nullpointer Exception :"+e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<Pokemon> call, Throwable t) {

                System.out.println("In Failure :" + t.getMessage());

            }
        });

    }

    public void generateView(ArrayList<Pokemon_> pokemonS, View view)
    {

        recyclerAdapter = new RecyclerAdapter(pokemonS, getActivity().getApplicationContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL,false);
        recyclerView = view.findViewById(R.id.main_recyclerView);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerAdapter.setOnClickListner(onClickListener);

    }

    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) v.getTag();
            int position = viewHolder.getAdapterPosition();

            Toast.makeText(getActivity().getApplicationContext(),parrayList.get(position).getName(),Toast.LENGTH_SHORT).show();

            Bundle b = new Bundle();
            b.putParcelable("pokemon",parrayList.get(position));
            b.getString("test","Check");

            navController.navigate(R.id.fragementFirst,b);




        }
    };
}
