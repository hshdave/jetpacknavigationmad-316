package com.example.jetpacknaavigation;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class FragementFirst extends Fragment {

    ImageView pimageView;
    TextView txt_name,txt_type,txt_ability,txt_height,txt_weight,txt_desc;
    Pokemon_  pokemon;

    public FragementFirst() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragement_first, container, false);


    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pimageView = view.findViewById(R.id.pd_imgv);
        txt_name = view.findViewById(R.id.pd_txtName);
        txt_type = view.findViewById(R.id.pd_txtType);
        txt_ability = view.findViewById(R.id.pd_txtAbility);
        txt_height = view.findViewById(R.id.pd_txtHeight);
        txt_weight = view.findViewById(R.id.pd_txtWeight);
        txt_desc = view.findViewById(R.id.pd_txtDesc);

        txt_desc.setMovementMethod(new ScrollingMovementMethod());

        pokemon = getArguments().getParcelable("pokemon");

        generateView();

    }

    public void generateView()
    {
        Picasso.get().load(pokemon.getImage()).into(pimageView);
        txt_name.setText("Name :"+pokemon.getName());
        txt_type.setText("Type :"+pokemon.getType());
        txt_ability.setText("Ability :"+pokemon.getAbility());
        txt_height.setText("Height :"+pokemon.getHeight());
        txt_weight.setText("Weight :"+pokemon.getWeight());
        txt_desc.setText("Description :"+pokemon.getDescription());
    }
}
